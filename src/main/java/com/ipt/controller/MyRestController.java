package com.ipt.controller;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ipt.exception.FormatException;
import com.ipt.exception.IncorrectRequestException;
import com.ipt.service.SlackService;
import com.ipt.vo.SlackResponse;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class MyRestController {

	private final SlackService slack;
	
	@PostMapping("/test")
	public ResponseEntity<String> test(){
		return new ResponseEntity<String>("connected", HttpStatus.ACCEPTED);
	}
	
	@PostMapping("/help")
	public SlackResponse help(HttpServletRequest req){
		return new SlackResponse("Command List\n/check YYYY MM DD\n/book YYYY MM DD HH MM HH MM");
	}
	
	@PostMapping("/check")
	public SlackResponse check(HttpServletRequest req){
		SlackResponse result;
		try {
			String text = req.getParameter("text").replaceAll(" ", "");
			result = slack.check(text);
		}catch(FormatException e) {
			result = new SlackResponse(e.getMessage() + " The correct request format is /check YYYY MM DD");
		}
		
		return result;
	}
	
	@PostMapping("/book")
	public SlackResponse book(HttpServletRequest req){
		SlackResponse result;
		try {
			String text = req.getParameter("text").replaceAll(" ", "");
			String name = req.getParameter("user_name");
			result = slack.book(text, name);
		}catch(FormatException e) {
			result = new SlackResponse(e.getMessage() + " The correct request format is YYYY MM DD HH MM HH MM");
		} catch (IncorrectRequestException e) {
			result = new SlackResponse(e.getMessage() + " A book in must be earlier than a book out. Booking must be equal or longer than 1 minute.");
		} catch (ParseException e) {
			result = new SlackResponse("The input could not be parsed properly.");
		}
		
		return result;
	}
}
