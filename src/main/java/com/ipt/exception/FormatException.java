package com.ipt.exception;

public class FormatException extends Exception{

	public FormatException() {
		super("Wrong format.");
	}
}
