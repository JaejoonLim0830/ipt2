package com.ipt.vo;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class SlackResponse {

	@NonNull
	private String text;
}
