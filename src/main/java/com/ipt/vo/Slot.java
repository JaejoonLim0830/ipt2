package com.ipt.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;

@Data
public class Slot {

	private Date start_date;
	private Date end_date;
	private String user_name;
	
	public Slot(String param, String name) throws ParseException {
		String dmy = param.substring(0,8);
		String st = param.substring(8,12);
		String ed = param.substring(12);
		
		DateFormat f = new SimpleDateFormat("yyyyMMddHHmm");
		this.start_date = f.parse(dmy+st);
		this.end_date = f.parse(dmy+ed);
		this.user_name = name;
	}
	
	public Slot() {
		
	}
	
	@Override
	public String toString() {
		DateFormat f = new SimpleDateFormat("HH:mm");
		String st = "From " + f.format(this.start_date);
		String ed = "To " + f.format(this.end_date);
		return st + " " + ed + " User name : " + this.user_name;
	}
}
