package com.ipt.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.ipt.exception.FormatException;
import com.ipt.vo.Slot;

@Mapper
public interface MyDao {

	public List<Slot> selectSlots(String text) throws FormatException;
	public int insertSlot(Slot slot) throws FormatException;
}
